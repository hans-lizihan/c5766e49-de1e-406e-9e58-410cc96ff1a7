import axios from 'axios'
import Vue from 'vue'

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line
  new Vue({
    el: '#app',

    data: {
      email: '',
      password: '',

      loading: false,
      message: '',
      details: []
    },

    methods: {
      onSubmit (event) {
        event.preventDefault()
        this.loading = true
        axios({
          method: 'POST',
          url: '/api/session',
          data: {
            email: this.email,
            password: this.password
          }
        })
          .then(response => {
            this.loading = false
            this.message = ''
            this.details = []
            window.location = '/settings'
          })
          .catch(e => {
            this.message = e.response.data.message
            this.details = e.response.data.details
            this.loading = false
          })
      }
    }
  })
})
