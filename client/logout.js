import axios from 'axios'

document.addEventListener('DOMContentLoaded', () => {
  axios({
    method: 'DELETE',
    url: '/api/session'
  })
    .then(response => {
      window.location = '/'
    })
    .catch(e => {
      console.error(e)
    })
})
