import Vue from 'vue'
import axios from 'axios'

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line
  new Vue({
    el: '#app',

    created () {
      this.loading = true
      axios({
        method: 'GET',
        url: '/api/user'
      })
        .then(response => {
          this.loading = false
          const date = new Date(response.data.date_of_birth)
          this.user = Object.assign(response.data, {
            date_of_birth: `${date.getFullYear()}-${prependZero(date.getMonth() + 1)}-${prependZero(date.getDate())}`
          })
        })
        .catch(e => {
          this.loading = false
          this.message = e.response.data.message
          this.details = e.response.data.details
        })
    },

    data: {
      message: '',
      details: [],
      user: {},
      loading: false
    },

    computed: {
      errors () {
        return this.details.reduce((acc, cur) => {
          acc[cur.property] = true
          return acc
        }, {})
      }
    },

    methods: {
      onSubmit (event) {
        event.preventDefault()
        this.loading = true
        axios({
          method: 'PATCH',
          url: '/api/user',
          data: Object.assign(this.user, {
            date_of_birth: this.user.date_of_birth ? new Date(this.user.date_of_birth).toISOString() : null
          })
        })
          .then(response => {
            this.loading = false
            this.user = response.data
            this.details = []
            this.message = ''
          })
          .catch(e => {
            this.loading = false
            this.message = e.response.data.message
            this.details = e.response.data.details
          })
      }
    }
  })
})

function prependZero (number) {
  if (number < 10) {
    return `0${number}`
  }
  return `${number}`
}
