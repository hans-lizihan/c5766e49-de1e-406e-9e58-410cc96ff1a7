import axios from 'axios'
import Vue from 'vue'

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line
  new Vue({
    el: '#app',

    data: {
      first_name: '',
      last_name: '',
      password: '',
      email: '',

      message: '',
      details: [],
      loading: false
    },

    methods: {
      onSubmit (event) {
        event.preventDefault()
        const policyCode = event.target.querySelector('#policy_code').value
        this.loading = true
        axios({
          method: 'POST',
          url: '/api/user',
          data: {
            first_name: this.first_name,
            last_name: this.last_name,
            password: this.password,
            email: this.email,
            policy_code: policyCode
          }
        })
          .then(response => {
            this.loading = false
            this.message = ''
            this.details = []
            window.location = '/'
          })
          .catch(e => {
            this.loading = false
            this.message = e.response.data.message
            this.details = e.response.data.details
          })
      }
    }
  })
})
