module.exports = {
  env: process.env.NODE_ENV || 'development',
  redis: {
    host: process.env.REDIS_HOST || '127.0.0.1',
    port: process.env.REDIS_PORT || 6379,
  }
};
