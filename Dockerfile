FROM node:8.11.4

RUN apt-get update && \
  yarn global add \
  node-gyp \
  && \
  yarn cache clean && \
  cd ~

RUN mkdir -p /deploy && \
  chown node:node /deploy

# General the build step , so everything ONBUILD
# app name
ENV WORK_DIR /deploy/app
ENV REDIS_HOST redis
ENV REDIS_PORT 6379

# Create app directory
RUN mkdir -p ${WORK_DIR}
WORKDIR ${WORK_DIR}

COPY ["yarn.lock", "package.json", "./"] .

# Install app dependencies
# --frozen-lockfile don't generate a lockfile and fail if an update is needed
RUN yarn install --prod --frozen-lockfile && \
  yarn cache clean

# Bundle app source
COPY . .

EXPOSE 8000

CMD ["yarn", "start"]
