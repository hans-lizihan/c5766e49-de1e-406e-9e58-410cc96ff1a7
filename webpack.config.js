const webpack = require('webpack');
const path = require('path');

const autoprefixer = require('autoprefixer');
const ManifestPlugin = require('webpack-manifest-plugin');

const env = process.env.NODE_ENV || 'development';

module.exports = {
  module: {
    rules: [
      {
        include: [path.resolve(__dirname, 'client')],
        loader: 'babel-loader',

        options: {
          plugins: ['syntax-dynamic-import'],

          presets: [
            [
              'env',
              {
                modules: false
              }
            ]
          ]
        },

        test: /\.js$/
      },
      {
        test: /\.css$/,

        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',

            options: {
              importLoaders: 1,
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',

            options: {
              plugins: function () {
                return [autoprefixer];
              }
            }
          }
        ]
      }
    ]
  },

  entry: {
    login: './client/login.js',
    register: './client/register.js',
    settings: './client/settings.js',
    logout: './client/logout.js'
  },

  output: {
    filename: env === 'development' ? '[name].js' : '[name].[chunkhash].js',
    path: path.resolve(__dirname, 'public')
  },

  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
    }
  },

  mode: env,

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          priority: -10,
          test: /[\\/]node_modules[\\/]/
        }
      },

      chunks: 'initial',
      minChunks: 1,
      minSize: 30000,
      name: 'commons'
    }
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(env),
      }
    }),
    new ManifestPlugin({
      publicPath: '/',
    })
  ]
};
