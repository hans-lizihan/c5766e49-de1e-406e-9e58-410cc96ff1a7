# Login Portal for prenetics challenge

<!-- MarkdownTOC -->

- Run it locally
- Database
  - Schema
  - Migration
- Server
  - Endpoints
  - UI
  - Security
  - Missing features
- Scaling up
  - Web server
  - Redis
  - Database
- Testing
  - Unit test
  - Integration test
  - e2e tests
- Environment Variables
- Start Development
- Build image and push to gitlab
- Deployment

<!-- /MarkdownTOC -->

## Run it locally

The easiest way to run this project is via docker-compose locally (if 1st time you see any error, just ctrl-c and run it again)

```
docker-compose up
```

## Database

I chose to use postgresql as database, basically to align with tech stack prenetics is actually doing right now.

### Schema

The database is being designed in a very naive way: store all information in only 1 table to simplify the whole process

| Value          | Format        | Default        | Nullable | Description                                                                             |
|:---------------|:--------------|:---------------|:---------|:----------------------------------------------------------------------------------------|
| id             | string (uuid) | generated uuid | False    | unique identifier of user                                                               |
| first_name     | varchar       | null           | True     | user first name                                                                         |
| last_name      | varchar       | null           | True     | user last name                                                                          |
| email          | varchar       | N/A            | False    | User's email address                                                                    |
| password       | varchar       | N/A            | False    | User's password                                                                         |
| date_of_birth  | varchar       | N/A            | False    | User's date of birth                                                                    |
| policy_code    | varchar(8)    | N/A            | False    | User's policy code                                                                      |
| genetic_result | json          | null           | True     | User's genetic result, since it's not clear how it's look like, will store it as a json |
| created_at     | timestamp     | N/A            | False    | standard created_at timestamp                                                           |
| udpated_at     | timestamp     | N/A            | False    | standard udpated_at timestamp                                                           |

### Migration

Migration is being catered by the `typeorm` library. to migrate run this command:

```
ts-node ./node_modules/.bin/typeorm migration:run
```

## Server

The api is built using `koa.js` with cookie/session support. The session store is using redis, de-facto way of being a session store it's performance is quite good and not possibility to reach bottle neck in a short amount of time.

### Endpoints

Please be noted that the resource `user` and `session` is not in usual plural form like `users` or `sessions`. This is due to its specialty as a 1-1 relation ship with each user.

Namely for each user he will only have 1 user resource and for each browser he will only obtain 1 session. When ever the user operate on this resource there are no need to operate on other user's resource thus the design goes to this way.

| method | url          | description                                       |
|:-------|:-------------|:--------------------------------------------------|
| get    | /api/user    | fetch user info                                   |
| post   | /api/user    | create a user and login the user                  |
| patch  | /api/user    | update user information, including genetic result |
| delete | /api/user    | remove the user account                           |
| post   | /api/session | create the session (for login the user)           |
| delete | /api/session | remove the session (for logout the user)          |

### UI

The UI is really easy so I did not over-engineer them using single page applications. It is simply built with Vue.js with pug template.

The UI route is completely being controlled by the backend.

| method | url       | description                                                                               |
|:-------|:----------|:------------------------------------------------------------------------------------------|
| get    | /login    | login form and request to `post /session`                                                 |
| get    | /register | register form and request to `post /user`                                                 |
| get    | /settings | a client for getting user via `get /api/user` and update the user via `delete /api/user/` |
| get    | /logout   | logout blank page for calling `delete /api/session`                                       |

### Security

The largest security issue should be the cookie-session auth mechanism.

Due to time restrict, I did not spent much time in that end. But there are definitely some todos to be catered:

1. cookie should have `httpOnly` flag to be not manipulated by client side javascript
2. cookie should have `secure` flag to be transported only on https network and application should use `app.proxy = true` to support ssl termination
3. default timeout for the session is 24 hours, depending on security requirements, we may need to reduce this time.
4. when a user changes his email address, we should have logged out the user session and make him login again.
5. when a user changes his email address, we should have logged out all session in the session store belongs to that user.
6. length of password and format should be defined and restricted.


### Missing features

1. remove user account
2. forget password functionality
3. email verification features
4. use oauth2 to protocol to support oauth

Especially for oauth2, given enough time I will definitely implement some endpoint, so that this portal is not only apply-able to this app, it could be shared among all application in the company.

Here is a tease for endpoint that signing JWT token and use a callback to feed it to the client. Doing so gives extremly high flexibility to decouple authentication logic with other application's business logic.

```
--> GET /authorize
    ?redirect_uri=https://another-application.prenetics.com/oauth/callback
    &response_type=token
    &audience=api.another-application.prenetics.com
    &scope=user
    &client_id=6127c55d-c267-4367-b667-5dc18ac6908f%20
    &prompt=none
    &connection=email

<-- GET https://another-application.com/oauth/callback
    ?access_token={xxxxx}
    &id_token={xxxxx}
```

## Scaling up

### Web server

If there were too much load to the application, we could horizontally scale up the application. Since this app is well containerized, we could easily deploy this thing to k8s clusters. There we could configure the HPA (horizontally pod autoscaler) to elasticity scale up. (currently it only support scale up based on CPU usage)

For k8s cluster, it's just a few clicks away in GCP to enable autoscale with GKE. For AWS there should be some configuration for the ec2 autoscaler (this I haven't tried out yet, will be nice to tune it if there were a chance).

### Redis

Most of the time redis will not be the bottle neck. Only issue is about single point of failure.

In such case there would be 2 strategy to ensure high availability for the redis.

1. self host it

Use a separated EC2 / GCE cluster to serve a `redis-sentinel`. we need 5 node for such case:

![redis-sentinel](https://gitlab.com/hans-lizihan/c5766e49-de1e-406e-9e58-410cc96ff1a7/uploads/28cfd484a0cb593e419de3bc67b21a45/redis-sentinel.png)

Could try to use k8s to set it up as well, https://github.com/tarosky/k8s-redis-ha

2. just use elastic cache / cloud cache...


### Database

The simplest way to scale up the DB is to put that in a hosted solution like aws RDS or GCP cloud sql. To scale them up, just click on UI and vertically add memory / cores of CPU.

If vertical scale is being pushed to a limit... well, congratulations! this application is really successful, and by that time the company must have enough $$ to hire a professional DBA, so how to shard / offload data / configure replica sets could all be solved by then.

For my experiences, unless there were absolutely necessity, I would not start to consider doing horizontally scale for database at the very beginning stage of development. As far as I could see, this user table will NOT be the type that need a horizontal scale solution at this time.

## Testing

Due to time restrains, I did not implemented testing in this challenge application. If there were enough time, i would've chose `jest` as the test run time sine it's the only all-in-one test framework on the market, and the API and speed is generally not bad.

### Unit test

We could test the model entity functionality and validation using unit tests.

e.g.

```javascript
const user = new User;
user.email = 'ab';
const validationErrors = validate(user);
expect(validationErrors.length).toEqual(1);
```

### Integration test

For integration test, I'd suggest to boot up a real redis server and real postgres server to run.

What i usually do is use `supertest` to boot up the server and really test agaist the endpoints.

e.g.

```
const app = supertest(server);
const email = faker.internet.email();
const password = faker.random.alphaNumeric(8);
const response = await app
  .post('/api/user')
  .send({
    email,
    password
  });
expect(response.status).toEqual(201);
expect(await UserRepo.findOne({email})).toBeTruthy();
```


### e2e tests

This should be the last resource for testing out this application. I would recommend `codecept`. I have tried out `puppeteer` for a while but it is really unstable and really hard to use.

example test will be look like this:

```
I.amOnPage(config.url + '/login')
I.fillField(input[name="email"], email)
I.fillField(input[name="password"], password)
I.click('Login')
I.waitForText(`Settings`)
I.see('Settings')
```

## Environment Variables

Sensitive information could all be passed in as environment variables, see options below

| Name             | Description                                                  |
|:-----------------|:-------------------------------------------------------------|
| TYPEORM_HOST     | default to `127.0.0.1` could override                        |
| TYPEORM_PORT     | default to `5432`                                            |
| TYPEORM_USERNAME | default to `test`, should change to secret on production env |
| TYPEORM_PASSWORD | default to `test` should change to secret on production env  |
| TYPEORM_DATABASE | default to `prenetics_identity`                              |
| REDIS_HOST       | defualt to '127.0.0.1', could override                       |
| REDIS_PORT       | default to `6379`, could override                            |
| NODE_ENV         | one of `production` `test` `development`                     |

## Start Development

to run this project, you should have node 8 installed on your host machine

this project depends on postgresql and redis as third party component. To develope run this project locally, please do

```
docker-compose -f docker-compose-stack.yml up # in another tab
yarn --pure-lockfile
yarn dev
```

## Build image and push to gitlab

to do this, you need to ask me to grant an access token.

then you do

```
docker login -u githlab-ci-token -p ${GIT_LAB_TOKEN} registry.gitlab.com
docker build -t registry.gitlab.com/hans-lizihan/c5766e49-de1e-406e-9e58-410cc96ff1a7 .
docker push registry.gitlab.com/hans-lizihan/c5766e49-de1e-406e-9e58-410cc96ff1a7
```

## Deployment

I aim to set this project for deploying to k8s, obviously this will be another todo item :joy:

But I will state what I am thinking here.

1. just use gitlab to do the CI/CD pipeline. - This is 1st time I use gitlab, and I am totally impressed by how powerful it is. I will explore more on how to build a docker image in gitlab CI.
2. use the default k8s binding to deploy to GKE.
3. enable auto-devOps so that it will be a bless to manage different environments.
