import { Context } from 'koa';

export const view = async (ctx: Context) => {
  await ctx.render('settings');
};
