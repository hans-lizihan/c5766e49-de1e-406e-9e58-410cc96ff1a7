import { Context } from 'koa';
import * as _ from 'lodash';

import { validate } from 'class-validator';
import { User } from '../entities/User';
import { getRepository } from 'typeorm';

export const create = async (ctx: Context) => {
  const UserRepo = getRepository(User);
  const {
    first_name,
    last_name,
    password,
    email,
    policy_code,
  } = <{
    first_name: string,
    last_name: string,
    password: string,
    email: string,
    policy_code: string,
  }>ctx.request.body;

  const user = new User();
  user.first_name = first_name;
  user.last_name = last_name;
  user.password = await User.hashPassword(password);
  user.email = email;
  user.policy_code = policy_code;

  const validationErrors = await validate(user);
  if (validationErrors.length > 0) {
    const details = validationErrors.map(error => ({
      property: error.property,
      messages: _.flatten(_.values(error.constraints)),
    }));
    ctx.throw(400, 'Invalid User', { details });
  }

  if (await UserRepo.findOne({ email })) {
    ctx.throw(409, 'User Alreay Exists');
  }

  const userSaved = await UserRepo.save(user);
  ctx.login(userSaved);
  ctx.status = 201;
  ctx.body = _.omit(userSaved, 'password');
};

export const find = async (ctx: Context) => {
  ctx.body = _.omit(await ctx.state.user, 'password');
};

export const update = async (ctx: Context) => {
  const {
    first_name,
    last_name,
    email,
    policy_code,
    date_of_birth,
    genetic_result,
  } = <{
    first_name: string,
    last_name: string,
    email: string,
    policy_code: string,
    date_of_birth: string,
    genetic_result: object,
  }>ctx.request.body;

  const UserRepo = getRepository(User);
  const user: User = ctx.state.user;
  user.first_name = first_name;
  user.last_name = last_name;
  user.email = email;
  user.policy_code = policy_code;
  user.genetic_result = genetic_result;
  user.date_of_birth = date_of_birth;

  const validationErrors = await validate(user);

  if (validationErrors.length > 0) {
    const details = validationErrors.map(error => ({
      property: error.property,
      messages: _.flatten(_.values(error.constraints)),
    }));
    ctx.throw(400, 'Invalid User', {
      details,
    });
  }
  const userToUpdate = {
    first_name,
    last_name,
    email,
    policy_code,
    genetic_result,
    date_of_birth,
  };

  ctx.body = _.omit(await UserRepo.update(user.id, userToUpdate), 'password');
};

export const remove = async (ctx: Context) => {
  const UserRepo = getRepository(User);
  await UserRepo.remove(ctx.state.user);
  ctx.status = 204;
};
