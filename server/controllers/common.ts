import { Context } from 'koa';

export const landing = (ctx: Context) => {
  if (ctx.isAuthenticated()) {
    ctx.redirect('/settings');
  } else {
    ctx.redirect('/login');
  }
};
