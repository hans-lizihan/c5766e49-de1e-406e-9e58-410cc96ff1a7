import { Context } from 'koa';
import * as passport from 'passport';
import * as _ from 'lodash';

export const create = (ctx: Context, next: () => Promise<void>) => {
  return passport.authenticate('local', async (err, user) => {
    if (err) throw err;
    await ctx.login(user);
    ctx.body = _.omit(user, 'password');
  })(ctx, next);
};

export const remove = async (ctx: Context) => {
  await ctx.logout();
  ctx.status = 204;
};
