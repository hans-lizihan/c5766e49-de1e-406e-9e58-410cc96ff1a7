import { Context } from 'koa';

const POLICY_CODE = 'ABCD1234';

export const login = async (ctx: Context) => {
  await ctx.render('login', {
    title: 'Prenetics Login',
  });
};

export const register = async (ctx: Context) => {
  // will default a policy_code in the controller fow now
  // if necessary move the `current policy code` in to a database
  // and use and endpoint to change it
  // so that we could change it easily when policy is being constantly changed
  await ctx.render('register', {
    policy_code: POLICY_CODE,
    title: 'Prenetics Register',
  });
};

export const logout = async (ctx: Context) => {
  await ctx.render('logout');
};
