import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import {
  IsEmail,
  IsNotEmpty,
  IsDateString,
  IsOptional,
  Length,
  IsJSON,
} from 'class-validator';

import * as bcrypt from 'bcrypt';

const SALT_ROUNDS = 10;
@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    nullable: true,
  })
  @IsOptional()
  first_name: string;

  @Column({
    nullable: true,
  })
  @IsOptional()
  last_name: string;

  @Column()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @Column()
  @IsNotEmpty()
  password: string;

  @Column({
    nullable: true,
  })
  @IsOptional()
  @IsDateString()
  date_of_birth: string;

  @Column({
    type: 'varchar',
    length: 8,
  })
  @Length(8, 8)
  policy_code: string;

  @Column({
    type: 'json',
    nullable: true,
  })
  @IsJSON()
  @IsOptional()
  genetic_result: object;

  @CreateDateColumn()
  created_at: string;

  @UpdateDateColumn()
  updated_at: string;

  static async hashPassword(password) {
    return bcrypt.hash(password, SALT_ROUNDS);
  }

  static async comparePasswords(inputPassword, dbPassword) {
    return bcrypt.compare(inputPassword, dbPassword);
  }
}
