import { MigrationInterface, QueryRunner } from 'typeorm';

export class User1535695048045 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
    CREATE TABLE "user" (
      "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
      "first_name" character varying,
      "last_name" character varying,
      "email" character varying NOT NULL,
      "password" character varying NOT NULL,
      "date_of_birth" character varying,
      "policy_code" character varying(8) NOT NULL,
      "genetic_result" json,
      "created_at" TIMESTAMP NOT NULL DEFAULT now(),
      "updated_at" TIMESTAMP NOT NULL DEFAULT now(),
      CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "user"`);
  }

}
