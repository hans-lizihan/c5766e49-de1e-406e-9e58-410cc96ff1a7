import 'reflect-metadata';
import './passports/index';

import { createConnection, getConnectionOptions, ConnectionOptions } from 'typeorm';

import * as config from 'config';
import * as path from 'path';
import * as Koa from 'koa';
import * as views from 'koa-views';
import * as session from 'koa-session';
import * as passport from 'koa-passport';
import * as koaStatic from 'koa-static';
import * as koaLogger from 'koa-logger';
import { viewRouter, apiRouter } from './router';
import loadManifest from './middlewares/loadManifest';
import handle4xxErrors from './middlewares/handle4xxErrors';
import handle5xxErrors from './middlewares/handle5xxErrors';
import handle404 from './middlewares/handle404';
import store from './utils/store';

const app = new Koa();
getConnectionOptions()
  .then((options: ConnectionOptions) => {
    return createConnection(Object.assign(options, {
      host: process.env.TYPEORM_HOST || (<any>options).host,
      port: process.env.TYPEORM_PORT || (<any>options).port,
      username: process.env.TYPEORM_USERNAME || (<any>options).username,
      password: process.env.TYPEORM_PASSWORD || (<any>options).password,
      database: process.env.TYPEORM_DATABASE || (<any>options).database,
      entities: [
        'build/entities/**/*.js',
      ],
      migrations: [
        'build/migrations/**/*.js',
      ],
      subscribers: [
        'build/subscribers/**/*.js',
      ],
    }));
  })
  .then(() => {
    app.keys = ['bc6ab21c-f440-415a-8c78-eb9ffcef489c', 'bc6ab21c-f440-415a-8c78-eb9ffcef489c'];
    const manifest = require(path.resolve(__dirname, '../public/manifest.json'));

    app
      .use(koaLogger())
      .use(koaStatic(path.resolve(__dirname, '../public')))
      .use(views(path.resolve(__dirname, 'views'), {
        extension: 'pug',
        options: {
          cache: config.get('env') === 'production' ? true : false,
          basedir: path.resolve(__dirname, 'views'),
        },
      }))
      .use(handle5xxErrors())
      .use(handle4xxErrors())
      .use(session({ store, key: 'prenetics:session' }, app))
      .use(passport.initialize())
      .use(passport.session())
      .use(loadManifest(manifest))
      .use(viewRouter.routes())
      .use(viewRouter.allowedMethods())
      .use(apiRouter.routes())
      .use(apiRouter.allowedMethods())
      .use(handle404())
      .listen(8000, () => console.log('Server running on 8000!'));
  });
