import { Context } from 'koa';

export default (redirectTo: string = '/') =>
  async (ctx: Context, next: () => Promise<void>) => {
    if (!ctx.isAuthenticated()) {
      return next();
    }
    ctx.redirect(redirectTo);
  };
