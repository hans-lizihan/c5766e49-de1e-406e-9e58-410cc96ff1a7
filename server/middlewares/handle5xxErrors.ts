import { Context } from 'koa';
import * as config from 'config';

export default () => async (ctx: Context, next: () => Promise<void>): Promise<void> => {
  try {
    await next();
  } catch (e) {
    console.error(e.stack);
    ctx.status = e.status || 500;
    if (ctx.get('content-type').includes('application/json')) {
      ctx.body = {
        message: e.message,
        details: e.details || [],
      };
    } else {
      if (config.get('env') !== 'production') {
        ctx.state.error = e.stack;
      }
      await ctx.render('500', {
        title: '500 - Internal Server Error',
      });
    }
  }
};
