import { Context } from 'koa';

export default () => async (ctx: Context, next: () => Promise<void>): Promise<void> => {
  try {
    await next();
  } catch (e) {
    if (e.status >= 400 && e.status < 500) {
      ctx.status = e.status;
      ctx.body = {
        message: e.message,
        details: e.details || [],
      };
    }

    throw e;
  }
};
