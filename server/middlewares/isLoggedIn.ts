import { Context } from 'koa';

export default (redirectTo: string = '/login') =>
  async (ctx: Context, next: () => Promise<void>) => {
    if (ctx.isAuthenticated()) {
      await next();
      return;
    }
    if (ctx.get('content-type').includes('application/json')) {
      ctx.throw(401, 'Please login to perform this operation');
    } else {
      ctx.redirect(redirectTo);
    }
  };
