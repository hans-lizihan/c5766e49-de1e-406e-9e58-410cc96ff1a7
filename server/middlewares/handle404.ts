import { Context } from 'koa';

export default () => async (ctx: Context) => {
  ctx.status = 404;
  if (ctx.get('content-type').includes('application/json')) {
    ctx.body = {
      message: 'Route not found',
      details: [],
    };
  } else {
    await ctx.render('404', {
      title: '404 - Page not found',
    });
  }
};
