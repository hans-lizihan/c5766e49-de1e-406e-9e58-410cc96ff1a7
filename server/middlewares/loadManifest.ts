import { Context } from 'koa';

export default (manifest: { [key: string]: string }) =>
  (ctx: Context, next: () => Promise<void>) => {
    ctx.state.manifest = manifest;
    return next();
  };
