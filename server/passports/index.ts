import * as passport from 'koa-passport';
import { User } from '../entities/User';
import { getRepository } from 'typeorm';
import local from './local';

/*
|--------------------------------------------------------------------------
| passport serialize and deserialize user function
|--------------------------------------------------------------------------
| serialize -> ctx.login(user) -- put user in session
| deserialize -> passport.authenticated() -- mount user object under ctx.state.user
| required for persistent login sessions
| passport needs ability to serialize and unserialize users out of session
|
*/

// Store the whole user object rather than just the Id
// used to serialize the user for the session
// only serialize the user with bare ID
passport.serializeUser((user: User, done) => {
  done(null, {
    id: user.id,
  });
});

// used to deserialize the user
passport.deserializeUser((user: { id: string }, done) => {
  const UserRepo = getRepository(User);
  UserRepo
    .findOne({ id: user.id })
    .then(realUser => done(null, realUser))
    .catch((err) => {
      done(err);
    });
});

/**
 * See the example to use in the controllers
 * @example
 * passport.authenticate('local', async (err, user) => {
 *   if (err) throw err;
 *   await ctx.login(user);
 *   ctx.body = {};
 * })(ctx, next);
 */
passport.use(local);
