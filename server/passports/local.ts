import { Strategy as LocalStrategy } from 'passport-local';
import * as httpError from 'http-errors';
import { getRepository } from 'typeorm';
import { User } from '../entities/User';

const options = {
  usernameField: 'email',
  passwordField: 'password',
};
const local = async (email: string, password: string, done) => {
  if (!email || !password) {
    done(httpError(422, 'Invalid Credentials'), null);
  }

  const UserRepo = getRepository(User);
  const user = await UserRepo.findOne({ email });
  if (user && await User.comparePasswords(password, user.password)) {
    done(null, user);
    return;
  }
  // unexpected error happen
  done(httpError(422, 'Invalid Credentials'), null);
};
export default new LocalStrategy(options, local);
