import * as Redis from 'ioredis';
import * as config from 'config';

const redis = new Redis({
  port: <number>config.get('redis.port'),
  host: <string>config.get('redis.host'),
});

export default redis;
