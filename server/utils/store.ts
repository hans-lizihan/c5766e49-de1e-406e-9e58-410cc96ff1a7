import redis from './redis';

const store = {
  async get(key: string, maxAge: number, { rolling }: { rolling: boolean }) {
    const sess = await redis.get(key);
    if (rolling) {
      await redis.expire(key, Math.floor(maxAge / 1000));
    }
    return JSON.parse(sess);
  },

  async set(
    key: string,
    sess: object,
    maxAge: number,
    { rolling, changed }: { rolling: boolean, changed: boolean },
  ) {
    if (changed) {
      await redis.set(key, JSON.stringify(sess));
    }
    if (rolling) {
      await redis.expire(key, Math.floor(maxAge / 1000));
    }
  },

  async destroy(key: string) {
    await redis.del(key);
  },
};

export default store;
