import * as Router from 'koa-router';
import * as bodyParser from 'koa-bodyparser';
import * as cors from '@koa/cors';

import * as user from './controllers/user';
import * as session from './controllers/session';
import * as auth from './controllers/auth';
import * as settings from './controllers/settings';
import * as common from './controllers/common';
import isLoggedIn from './middlewares/isLoggedIn';
import isLoggedOut from './middlewares/isLoggedOut';

export const viewRouter = new Router()
  .use(bodyParser())
  .get('/', common.landing)
  .get('/login', isLoggedOut(), auth.login)
  .get('/register', isLoggedOut(), auth.register)
  .get('/logout', isLoggedIn(), auth.logout)
  .get('/settings', isLoggedIn(), settings.view);

export const apiRouter = new Router({ prefix: '/api' })
  .use(bodyParser())
  .use(cors())
  .get('/user', isLoggedIn(), user.find)
  .post('/user', user.create)
  .patch('/user', isLoggedIn(), user.update)
  .delete('/user', isLoggedIn(), user.remove)
  .post('/session', session.create)
  .delete('/session', isLoggedIn(), session.remove);
